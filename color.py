import numpy as np


class Color:
    r: int
    g: int
    b: int

    def __init__(self, r: int, g: int, b: int):
        self.r = r
        self.g = g
        self.b = b

    def rgb(self):
        return np.array([self.r, self.g, self.b])
