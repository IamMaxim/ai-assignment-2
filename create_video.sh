#!/bin/sh

ffmpeg -pattern_type glob -i 'results/extracted/result*.png' -vcodec libx264 -pix_fmt yuv420p result.mp4
