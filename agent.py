import math
import random
from typing import List

import numba
import numpy as np

from constants import *
from line import weighted_line
from vector import Vector


class AgentState:
    move_factor = 10
    color_factor = 0.2
    cur_chunk_threshold = 1000
    cur_chunk_move_factor = 100
    cur_chunk_random_offset_factor = 6
    kickstart_factor = 1
    kickstart_threshold = 1000


class Coster:
    diff_image: np.array
    costs = np.zeros((CHUNK_DIM, CHUNK_DIM))

    def __init__(self, diff_image: np.array):
        self.diff_image = diff_image
        self.invalidate_region(0, 0, IMAGE_SIZE - 1, IMAGE_SIZE - 1)

    def compute_chunk_cost(self, chunk: Vector):
        A = self.diff_image[
            chunk.ix * CHUNK_SIZE: (chunk.ix + 1) * CHUNK_SIZE,
            chunk.iy * CHUNK_SIZE: (chunk.iy + 1) * CHUNK_SIZE,
            ]
        cost = (np.square(A)).sum()
        if cost < 0:
            print("ACHTUNG! COST < 0!!!", cost)
        return cost

    def invalidate_region(self, lx, ly, ux, uy):
        for x in range(math.floor(lx / CHUNK_SIZE), math.floor(ux / CHUNK_SIZE) + 1):
            for y in range(math.floor(ly / CHUNK_SIZE), math.floor(uy / CHUNK_SIZE) + 1):
                self.costs[x, y] = self.compute_chunk_cost(Vector(x, y))

    def chunk_cost(self, chunk: Vector) -> float:
        return self.costs[chunk.ix, chunk.iy]

    def total_cost(self):
        return self.costs.sum().item()


class Agent:
    pos: Vector = Vector(0, 0)
    chunkPos: Vector
    state: AgentState
    coster: Coster
    orig_image: np.array
    diff_image: np.array
    location_history: List[np.array] = []
    location_trend = Vector(0, 0)
    kickstarts_count = 0
    local_moves_count = 0
    global_moves_count = 0

    def __init__(self, orig_image: np.array, state: AgentState):
        self.orig_image = orig_image
        self.diff_image = -orig_image.astype('int64')
        self.state = state

        self.coster = Coster(self.diff_image)
        self.updateChunkPos()

    def updateChunkPos(self):
        self.chunkPos = self.pos // CHUNK_SIZE

    @staticmethod
    @numba.njit()
    def _compute_area_info(area: np.array, rel_pos: np.array):
        cur_chunk_cost = 0
        cur_chunk_dir = np.zeros(2)

        for x in range(area.shape[0]):
            for y in range(area.shape[1]):
                cost = (np.square(area[x, y])).sum()
                cur_chunk_cost += cost
                dx = x - rel_pos[0]
                dy = y - rel_pos[1]
                v = np.array([
                    math.exp(- 1 / 4 * dx) * cost,
                    math.exp(- 1 / 4 * dy) * cost,
                ])
                cur_chunk_dir += v

        return cur_chunk_cost, cur_chunk_dir

    def compute_move_dir(self) -> Vector:
        """
        Use weighted cost of each chunk to decide which direction to move in.
        :return: move vector
        """
        cur_pos = self.pos

        # Explore current chunk
        cur_area = self.diff_image[
                   max(cur_pos.x - CHUNK_SIZE // 2, 0): min(cur_pos.x + CHUNK_SIZE // 2, IMAGE_SIZE - 1),
                   max(cur_pos.y - CHUNK_SIZE // 2, 0): min(cur_pos.y + CHUNK_SIZE // 2, IMAGE_SIZE - 1),
                   ]
        rel_pos = np.array([
            cur_pos.x - max(cur_pos.x - CHUNK_SIZE // 2, 0),
            cur_pos.y - max(cur_pos.y - CHUNK_SIZE // 2, 0),
        ])

        cur_chunk_cost, cur_chunk_dir = Agent._compute_area_info(cur_area, rel_pos)
        cur_chunk_dir = Vector.fromNP(cur_chunk_dir)

        if cur_chunk_cost > self.state.cur_chunk_threshold:
            self.local_moves_count += 1
            return (cur_chunk_dir.norm() * random.random() * self.state.cur_chunk_move_factor +
                    Vector(random.random() * self.state.cur_chunk_random_offset_factor,
                           random.random() * self.state.cur_chunk_random_offset_factor)
                    ).intized

        dir = Vector(0, 0)
        for x in range(CHUNK_DIM):
            for y in range(CHUNK_DIM):
                cost = self.coster.chunk_cost(Vector(x, y))
                change = Vector(
                    1 / ((x - cur_pos.x // CHUNK_SIZE + 0.5) * CHUNK_SIZE) * cost,
                    1 / ((y - cur_pos.y // CHUNK_SIZE + 0.5) * CHUNK_SIZE) * cost,
                )
                dir += change

        self.global_moves_count += 1
        return (dir.norm() * random.random() * self.state.move_factor).intized

    def draw_line(self, point1: Vector, point2: Vector, color: np.array):
        weight = 2

        rr, cc, val = weighted_line(
            point1.ix, point1.iy,
            point2.ix, point2.iy,
            weight
        )

        rr = rr.clip(0, IMAGE_SIZE - 1)
        cc = cc.clip(0, IMAGE_SIZE - 1)

        color = (color * self.state.color_factor).astype('int16')
        self.diff_image[rr, cc] += color

        lx = min(point1.x, point2.x)
        ux = max(point1.x, point2.x)
        ly = min(point1.y, point2.y)
        uy = max(point1.y, point2.y)
        self.coster.invalidate_region(lx, ly, ux, uy)

    def step(self):
        self.location_trend = self.location_trend + (self.pos - self.location_trend) / 100
        trend_diff = self.pos - self.location_trend
        if trend_diff.len2() < self.state.kickstart_threshold:
            self.pos = Vector(random.random() * IMAGE_SIZE, random.random() * IMAGE_SIZE).intized
            self.kickstarts_count += 1

        dir = self.compute_move_dir()
        new_pos = self.pos + dir
        new_pos = new_pos.clamp(0, 0, IMAGE_SIZE - 1, IMAGE_SIZE - 1)
        self.draw_line(self.pos, new_pos, -self.diff_image[self.pos.ix, self.pos.iy])
        self.location_history.append(self.pos.np())
        self.pos = new_pos

    def out_image(self):
        return self.orig_image + self.diff_image
