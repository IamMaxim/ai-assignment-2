#!/bin/sh


for f in results/result_*; do
    if [ -f "$f/cost" ]; then
        id="$(echo $f | cut -d'_' -f 2)"
        echo "Cost of step ${id}: \t$(cat $f/cost)"
    fi
done



