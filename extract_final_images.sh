#!/bin/sh

rm -rf "results/extracted_final"
mkdir "results/extracted_final"

echo "Extracting final images..."
for f in results/result_*; do
    # Find the latest file in the sample results folder
    unset -v last_png
    for file in "$f"/*.png; do
        [[ $file -nt $last_png ]] && last_png=$file
    done

    id="$(echo $f | cut -d'_' -f 2)"
    cp "$last_png" "results/extracted_final/result_${id}.png"
done
echo "Done. Enjoy!"



