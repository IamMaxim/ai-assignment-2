import math
import numpy as np


class Vector:
    x: float
    y: float

    @property
    def ix(self):
        return int(self.x)

    @property
    def iy(self):
        return int(self.y)

    @property
    def intized(self):
        """
        :rtype: Vector
        :return: vector with coords converted to int
        """
        return Vector(self.ix, self.iy)

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return "(%s, %s)" % (self.x, self.y)

    def __add__(self, vec):
        """
        Returns new vector with 2 this and passed coords summed.

        :type vec: Union[Vector, float]
        """
        if type(vec) == Vector:
            return Vector(self.x + vec.x, self.y + vec.y)
        else:
            return Vector(self.x + vec, self.y + vec)

    def __sub__(self, vec):
        """

        :type vec: Union[Vector, float]
        """
        if type(vec) == Vector:
            return Vector(self.x - vec.x, self.y - vec.y)
        else:
            return Vector(self.x - vec, self.y - vec)

    # def __truediv__(self, other):
    #     return Vector(self.x / other, self.y / other)

    def __floordiv__(self, other: float):
        """

        :rtype: Vector
        """
        return Vector(self.x // other, self.y // other)

    def __truediv__(self, other):
        """

        :rtype: Vector
        """
        return Vector(self.x / other, self.y / other)

    def __iadd__(self, other):
        """

        :rtype: Vector
        """
        self.x += other.x
        self.y += other.y
        return self

    def __isub__(self, other):
        """

        :rtype: Vector
        """
        self.x -= other.x
        self.y -= other.y
        return self

    def __mul__(self, other):
        """

        :rtype: Vector
        """
        return Vector(self.x * other, self.y * other)

    def len2(self):
        return self.x * self.x + self.y * self.y

    def norm(self):
        """

        :rtype: Vector
        """
        s = math.sqrt(self.x * self.x + self.y * self.y)
        return Vector(self.x / s, self.y / s)

    def clamp(self, lx, ly, ux, uy):
        """

        :rtype: Vector
        """
        n = Vector(self.x, self.y)
        if n.x < lx:
            n.x = lx
        elif n.x > ux:
            n.x = ux
        if n.y < ly:
            n.y = ly
        elif n.y > uy:
            n.y = uy
        return n

    def np(self):
        return np.array([self.x, self.y])

    @classmethod
    def fromNP(cls, cur_chunk_dir):
        return Vector(cur_chunk_dir[0], cur_chunk_dir[1])

    @classmethod
    def fromTuple(cls, param):
        return Vector(param[0], param[1])
