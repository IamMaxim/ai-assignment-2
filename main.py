import datetime
import os
import sys
import imageio
import matplotlib
from agent import Agent, AgentState


def output_image_only(id, i):
    matplotlib.image.imsave("results/result_%06d/result_%06d.png" % (id, i),
                            agent.out_image().clip(0, 255).astype('uint8'))


orig_image = imageio.imread('image_3_512.png')
# Omit the alpha channel of an image if needed
if orig_image.shape[2] == 4:
    orig_image = orig_image[:, :, :-1]

# Read the agent parameters from the arguments
id = int(sys.argv[1])
state = AgentState()
state.move_factor = float(sys.argv[2])
state.color_factor = float(sys.argv[3])
state.cur_chunk_threshold = float(sys.argv[4])
state.cur_chunk_move_factor = float(sys.argv[5])
state.cur_chunk_random_offset_factor = float(sys.argv[6])
state.kickstart_factor = float(sys.argv[7])
state.kickstart_threshold = float(sys.argv[8])

# Create the results directory
os.mkdir('results/result_%06d' % id)

agent = Agent(orig_image, state)

start_time = datetime.datetime.now()
last_output = (start_time - start_time).total_seconds()
i = 0
while True:
    agent.step()

    cur_time = datetime.datetime.now()
    diff = (cur_time - start_time).total_seconds()
    # Output image ≈ every 1 minute
    if diff - last_output > 60:
        output_image_only(id, i)
        last_output = diff

    # Run for 5 minutes
    if diff > 5 * 60:
        break

    i += 1

# Save final image
output_image_only(id, i)

# Save final costs
cost = agent.coster.total_cost()
result_file = open('results/result_%06d/cost' % id, 'w+')
result_file.write('%s' % cost)
result_file.close()

# Save values of params
result_file = open('results/result_%06d/params' % id, 'w+')
result_file.write('%s' % ' '.join(sys.argv[1:]))
