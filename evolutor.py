import os
import random
import shutil
from subprocess import Popen
from typing import List

GEN_COUNT = 1000
POP_SIZE = 4

params = [
#   [name, init_min, init_max, decr_factor, incr_factor]
    ['move_factor', 0, 10, 0.8, 3],
    ['color_factor', 0, 0.5, 0.5, 1.5],
    ['cur_chunk_threshold', 0, 1000000, 0.5, 1.5],
    ['cur_chunk_move_factor', 0, 20, 0.5, 1.5],
    ['cur_chunk_random_offset_factor', 0, 10, 0.5, 1.5],
    ['kickstart_factor', 0, 1000, 0.5, 1.5],
    ['kickstart_threshold', 0, 10000, 0.5, 1.5],
]

best_sample = 10000000000000000
best_sample_params = []

if __name__ == '__main__':
    # Recreate results dir
    try:
        shutil.rmtree('results')
    except FileNotFoundError:
        pass
    os.mkdir('results')

    # Run GEN_COUNT generations
    for generation in range(GEN_COUNT):
        print('Running generation %s' % generation)
        population: List[List] = []

        # POP_SIZE samples in each generation.
        if generation == 0:
            # No data is available yet, the first generation; Generate some new data.
            for j in range(POP_SIZE):
                state = []
                # Add ID
                state.append(generation * POP_SIZE + j)
                for p in params:
                    val = p[1] + random.random() * (p[2] - p[1])
                    state.append(val)
                population.append(state)
        else:
            # Data from previous generation is available. Use it to generate new samples.
            population.append(best_sample_params)
            population[0][0] = generation * POP_SIZE
            for j in range(1, POP_SIZE):
                state = []
                # Add ID
                state.append(generation * POP_SIZE + j)
                for i, par in enumerate(params):
                    factor = par[3] + random.random() * (par[4] - par[3])
                    val = best_sample_params[i + 1] * factor
                    state.append(val)
                population.append(state)

        # Run the algorithm!
        processes = []
        for p in population:
            print(p)
            # process = Popen(['python3', 'main.py'] + [str(s) for s in p], stdin=DEVNULL, stdout=DEVNULL)
            process = Popen(['python3', 'main.py'] + [str(s) for s in p])
            processes.append(process)

        for p in processes:
            p.wait()

        start_id = generation * POP_SIZE
        end_id = start_id + POP_SIZE
        costs = []
        for i in range(start_id, end_id):
            cost_file = open('results/result_%06d/cost' % i, 'r')
            cost = float(cost_file.readline())
            cost_file.close()
            print('Cost of ID %s is %.1f' % (i, cost))
            costs.append(cost)

        for i, cost in enumerate(costs):
            if cost < best_sample:
                best_sample = cost
                best_sample_params = population[i]
