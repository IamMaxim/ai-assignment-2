#!/bin/sh

rm -rf "results/extracted"
mkdir "results/extracted"

echo "Extracting..."
for f in results/result_*; do
    for f2 in "$f"/*.png; do
        id="$(echo $f | cut -d'_' -f 2)"
        i="$(echo ${f2##*/} | cut -d'_' -f 2)"
        cp "$f2" "results/extracted/result_${id}_$i"
    done
done
echo "Done. Enjoy!"


